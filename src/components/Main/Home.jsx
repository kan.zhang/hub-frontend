import React from 'react';
import { Link } from 'react-router-dom';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = { postcode: "" };
  }

  handleInputChange = (e) => {
this.setState({postcode:e.target.value})
  }


  render() {
    return (
      <div className="home__frame">
        <div className="container search__box">
          <div className='row row__space--home'>
            <div className="col">
              <h1 className='home__title'>Find Your Dream Hub</h1>
            </div>
          </div>
          <form>
            <div className='row row__space--home d-flex justify-content-center'>
              <div className="col-8">
                <input
                  type="text"
                  className="form-control input__search"
                  placeholder="Please type postcode here, e.g. 4068"
                  onChange={this.handleInputChange}
                  value={this.state.postcode}
                ></input>
              </div>
            </div>
            <div className='row row__space--home'>
              <div className="col">
                <Link 
                to ={{
                  pathname: "/search",
                  state: {postcode:this.state.postcode}}}
                className="search__button">
                  Search
                </Link>
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}