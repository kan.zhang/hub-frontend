import React from 'react';


function SearchItem(props) {
    return (
        <li className="list-group-item">
            <div className="card-body">
                <h5 className="card-title text-info">Title: {props.title}</h5>
                <p className="card-text">
                    <span className="col-4 searchlist__price" >Price : ${props.price} {props.rentpaytype}</span>
                    <span className="col-4 searchlist__price">Type : {props.housetype}</span>
                    <span className="col-4 searchlist__price">Contact:{props.contact}</span>
                    <br></br>Address : {props.unit}, {props.stNo} {props.stName}, {props.suburb}, {props.state} {props.postcode}</p>
                <p>“ {props.content}”</p>
            </div> </li>
    )
}
export default SearchItem;