import React from "react";
import axios from "axios";
import SearchItem from '../Main/SearchItem';

import BlockUi from 'react-block-ui';
import { Loader} from 'react-loaders';
import 'loaders.css/loaders.min.css';
export default class MyPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            properties: [],
            postcode: "",
            isLoading:false
        }
    }
    componentDidMount() {
        this.setState({ isLoading: true });
        this.getSearchProperties();
    }
    getSearchProperties = () => {
        const link = '/property';
        axios.get(link)
            .then(res => {
                const postcode = this.props.location.state.postcode;
                const properties = res.data.filter(
                    (property) => property.address.postcode === +postcode);
                if (properties.length === 0) {
                    alert("😳😳😳Sorry, there is no property whose postcode is what you type🌇🌄🌅🌃");
                    this.props.history.replace('/');
                    return;
                }
                this.setState({
                    properties, postcode,
                    isLoading:false
                });
                //console.log(this.state);
            })
    }
    render() {
        return (
            <div className="col-10 post__info--box">
            <BlockUi
          tag="div"
          blocking={this.state.isLoading}
          message="Loading, please wait..."
          loader={<Loader active type='line-scale-party'
          color="#0074d9" />}
          >
                <form action="">
                    <div className="card">
                    <ul className="list-group">

                        {this.state.properties.map(
                            property => {
                                return (
                                    <SearchItem
                                        key={property._id}
                                        title={property.title}
                                        unit={property.address.unitNo}
                                        stNo={property.address.streetNo}
                                        stName={property.address.streetName}
                                        suburb={property.address.suburbName}
                                        state={property.address.stateName}
                                        postcode={property.address.postcode}
                                        contact={property.contact}
                                        content={property.content}
                                        price={property.price}
                                        housetype={property.type}
                                        rentpaytype={property.paymentInterval}
                                    />)
                            })}
                            </ul>
                    </div>
                </form>
                </BlockUi>
            </div>
        );
    }
}
