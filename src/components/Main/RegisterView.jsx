import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';

// import {register} from '../../api/auth';

class RegisterView extends Component {
    constructor(props) {
        super(props);
        this.state = {
          isFetching: false,
          email: '',
          password: '',
        };
    }

    handleInputChange = (event) => {
        const target = event.target;
        this.setState({
          [target.name]: target.value,
        });
    }

    handleSubmit =(event) => {
        event.preventDefault();
        // const {email, password} = this.state;
        //const {location} =this.props;

        this.setState({isFetching: true});
        // login(email, password).then(token => {
        //     this.setState({isFetching: false});
        //     this.props.history.replace('/mypost');
        //    // this.props.history.push(location.state ? location.state.from : "/");
        // });
    }
    

    render() {
        const {
          //isFetching, 
          email, password} = this.state;
        return (
          <div className="signinregister-container">
            <form className="jr-form-signin" onSubmit={this.handleSubmit}>
              User Name
              <label htmlFor="inputEmail" className="sr-only">
                Email address
              </label>
              <input
                type="email"
                name="email"
                className="form-control mb-3"
                placeholder="Email address"
                value={email}
                onChange={this.handleInputChange}
                required
                autoFocus
              />
              Password
              <label htmlFor="inputPassword" className="sr-only">
                Password
              </label>
              <input
                type="password"
                name="password"
                className="form-control mb-3"
                value={password}
                onChange={this.handleInputChange}
                placeholder="Password"
                required
              />
              Retype Password
              <label htmlFor="inputPassword" className="sr-only">
                Password
              </label>
              <input
                type="password"
                name="password"
                className="form-control mb-5"
                value={password}
                onChange={this.handleInputChange}
                placeholder="Password"
                required
              />
              <button className="btn btn-danger w-100">Register</button> 
            </form>
          </div>
        );
      }
    
}

export default withRouter(RegisterView)