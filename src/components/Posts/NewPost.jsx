import React from "react";
import axios from "axios";
import { Link} from 'react-router-dom';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
export default class NewPost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      unit: "",
      stNo: "",
      stName:"",
      suburb:"",
      state:"",
      postcode:"",
      contact: "",
      content: "",
      price: "",
      housetype: "",
      rentpaytype:""
    };
  }

  handleSubmit=(e)=> {
    const property= {
      "address": {
        "unitNo": this.state.unit,
        "streetNo": this.state.stNo,
        "streetName": this.state.stName,
        "suburbName": this.state.suburb,
        "stateName": this.state.state,
        "postcode": this.state.postcode
    },
    "images": [
        " ",
    ],
    "contact": this.state.contact,
    "title": this.state.title,
    "price": this.state.price,
    "type": this.state.housetype,
    "bedrooms": "1",
    "bathrooms": "1",
    "carpark": "1",
    "paymentInterval": this.state.rentpaytype,
    "content": this.state.content,
    "user":"5d8c6e87eb711f0d209b0fe9"
    };
    const link = "/user/5d8c6e87eb711f0d209b0fe9/properties/";
    //console.log(property);
    axios.post(link, property)
    .then(() => {
      this.props.history.replace('/mypost');
    })
    .catch((err) => console.log(err));

    e.preventDefault();
  }

  change = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    return (
      <Form className="container newpost__frame">
        <Form.Row>
          <Form.Group controlId="formSideButton" className="col-2 post__button--box">
            <Link to="/newpost" ><Button className="side__button red">New Post</Button></Link>
            <Link to="/mypost" ><Button className="side__button">My Post</Button></Link>
            <Link to="/favourites" ><Button className="side__button">Favourites</Button></Link>
          </Form.Group>
        <Form onSubmit={this.handleSubmit} className="col-10 post__info--box">
          <Form.Group controlId="formBasicTitle">
            <Form.Row className="top--space">
              <Form.Label className="col-2 center">Title</Form.Label>
              <Form.Control
                className="col-10"
                name="title"
                type="text"
                placeholder="Type title here please"
                value={this.state.title}
                onChange={e => this.change(e)}
              />
            </Form.Row>
          </Form.Group>
          <Form.Group controlId="formBasicAddress">
            
            <Form.Row>
              <Form.Label className="col-2 center">Address</Form.Label>
              <Form.Label className="col-1 small__font">Unit No.</Form.Label>
              <Form.Control
                className="col-1"
                name="unit"
                type="text"
                placeholder="Num"
                value={this.state.unit}
                onChange={e => this.change(e)}
              />
              <Form.Label className="col-1 small__font">St No.</Form.Label>
              <Form.Control
                className="col-1"
                name="stNo"
                type="text"
                placeholder="Num"
                value={this.state.stNo}
                onChange={e => this.change(e)}
              />
              <Form.Label className="col-1 small__font">St Name</Form.Label>
              <Form.Control
                className="col-5"
                name="stName"
                type="text"
                placeholder="Street name"
                value={this.state.stName}
                onChange={e => this.change(e)}
              />
            </Form.Row>
            <Form.Row>
              <Form.Label className="col-2"> </Form.Label>
              <Form.Label className="col-1 small__font center">Suburb</Form.Label>
              <Form.Control
                className="col-3"
                name="suburb"
                type="text"
                placeholder="Suburb name"
                value={this.state.suburb}
                onChange={e => this.change(e)}
              />
              <Form.Label className="col-1 small__font center">State</Form.Label>
              <Form.Control
                className="col-2"
                name="state"
                type="text"
                placeholder="State name"
                value={this.state.state}
                onChange={e => this.change(e)}
              />
              <Form.Label className="col-1 small__font center">P.C.</Form.Label>
              <Form.Control
                className="col-2"
                name="postcode"
                type="text"
                placeholder="Postcode"
                value={this.state.postcode}
                onChange={e => this.change(e)}
              />
            </Form.Row>
          </Form.Group>
            
          <Form.Group controlId="formBasicContact">
            <Form.Row>
              <Form.Label className="col-2 center">Contact</Form.Label>
              <Form.Control
                className="col-4"
                name="contact"
                type="text"
                placeholder="Number"
                value={this.state.contact}
                onChange={e => this.change(e)}
              />
              <Form.Label className="col-2 center">Price</Form.Label>
              <Form.Control
                className="col-4"
                name="price"
                type="text"
                placeholder="$"
                value={this.state.price}
                onChange={e => this.change(e)}
              />
            </Form.Row>
          </Form.Group>

          <Form.Row className="justify-content-center">
            <Form.Group className="col-4" controlId="form.ControlSelectHouseType">
              <Form.Label>House Type</Form.Label>
              <Form.Control as="select" name="housetype" value={this.state.housetype} onChange={e => this.change(e)}>
                <option>None</option>
                <option>Apartment</option>
                <option>Unit</option>
                <option>House</option>
                <option>Townhouse</option>
              </Form.Control>
            </Form.Group>
            <Form.Group className="col-4" controlId="form.ControlSelectRentPayTpye">
              <Form.Label>Rent Pay Type</Form.Label>
              <Form.Control as="select" name="rentpaytype" value={this.state.rentpaytype} onChange={e => this.change(e)}>
                <option>None</option>
                <option>Per Week</option>
                <option>Per Fortnight</option>
                <option>Per Month</option>
              </Form.Control>
            </Form.Group>
          </Form.Row>
        
          <Form.Group controlId="formBasicContent">
            <Form.Label className="col-12 center">Content</Form.Label>
            <Form.Control
              className="col-12"
              name="content"
              as="textarea" 
              rows="3"
              placeholder="Your content"
              value={this.state.content}
              onChange={e => this.change(e)}
            />
          </Form.Group>
        
          <Form.Row className="justify-content-end">
            <Button variant="primary" type="submit" className="submit__button">
              <span className="submit__button--font">Submit</span> 
            </Button>
          </Form.Row>
        </Form>
        </Form.Row>
      </Form>
    );
  }
}
