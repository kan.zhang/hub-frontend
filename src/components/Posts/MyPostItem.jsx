import React from "react";
import { Link } from 'react-router-dom';

function MyPostItem(props) {
    return (
        <div >
            <ul className="list-group">
                <li className="list-group-item">
                    <div>Price: {props.price} {props.rentpaytype}
                    </div>
                    <div>Address:
                        <span> {props.unit}, </span>
                        <span>{props.stNo} </span>
                        <span>{props.stName} St, </span>
                        <span>{props.suburb}, </span>
                        <span>{props.state}, </span>
                        <span>{props.postcode}</span>
                    </div>
                    <Link
                        to={{
                            pathname: `/editpost/${props.id}`,
                            state: {
                                title: props.title,
                                unit: props.unit,
                                stNo: props.stNo,
                                stName: props.stName,
                                suburb: props.suburb,
                                state: props.state,
                                postcode: props.postcode,
                                contact: props.contact,
                                content: props.content,
                                price: props.price,
                                housetype: props.housetype,
                                rentpaytype: props.rentpaytype,
                                id:props.id
                            }
                            
                        }}

                    >
                        <button className="submit__button">
                            <span className="submit__button--font">Edit</span>
                        </button>
                    </Link>
                    <Link to="/mypost">
                        <button onClick={() => props.handleRemove(props.id)} className="submit__button">
                            <span className="submit__button--font">Remove</span>
                        </button>
                    </Link>
                </li>
            </ul>


        </div>
    );
}
export default MyPostItem;