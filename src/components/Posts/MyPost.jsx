import React from "react";
import { Link } from 'react-router-dom';
import axios from "axios";
import BlockUi from 'react-block-ui';
import { Loader} from 'react-loaders';
import 'loaders.css/loaders.min.css';

import MyPostItem from './MyPostItem';
import Button from "react-bootstrap/Button";
export default class MyPost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      properties: [],
      isLoading:false
    }
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    this.getProperties();
  }
  getProperties = () => {
    const link = '/user/5d8c6e87eb711f0d209b0fe9/properties/'
    axios.get(link)
      .then(res => {
        //console.log(res);
        const properties = res.data.data.map(data => {
          return {
            key: data._id,
            price: data.price,
            title: data.title,
            content: data.content,
            contact: data.contact,
            housetype: data.type,
            rentpaytype: data.paymentInterval,
            unit: data.address.unitNo,
            stNo: data.address.streetNo,
            stName: data.address.streetName,
            suburb: data.address.suburbName,
            state: data.address.stateName,
            postcode: data.address.postcode,

          };
        });
        this.setState({ properties,
          isLoading:false
         });
        //console.log(properties);
      })
      .catch((err) => console.log(err));
  }

  handleRemove = (property) => {
    if (window.confirm("😊Do you want to delete this property🏠?")) {
      const link = `/user/5d8c6e87eb711f0d209b0fe9/properties/${property}`;
      axios.delete(link)
        .then(() => this.getProperties())
        .then(this.props.history.replace('/mypost'))
        .catch((err) => console.log(err));
    }
  }



  render() {
    return (
      <div className="container newpost__frame">
        <div className="row">
          <div className="col-2 post__button--box">
            <div className="col-12 right__side">
              <Link to="/newpost" >
                <Button className="side__button ">New Post</Button>
              </Link>
            </div>
            <div className="col-12 right__side">
              <Link to="/mypost" >
                <Button className="side__button red">My Post</Button>
              </Link>
            </div>
            <div className="col-12 right__side">
              <Link to="/favourites" >
                <Button className="side__button ">Favourites</Button>
              </Link>
            </div>
          </div>
          <div className="col-10 post__info--box">
          <BlockUi
          tag="div"
          blocking={this.state.isLoading}
          message="Loading, please wait..."
          loader={<Loader active type='line-scale-party'
          color="#0074d9" />}
          >
            <form action="">
              <div className="card">
                {this.state.properties.map(data => (
                  <MyPostItem
                    key={data.key}
                    price={data.price}
                    unit={data.unit}
                    stNo={data.stNo}
                    stName={data.stName}
                    suburb={data.suburb}
                    state={data.state}
                    postcode={data.postcode}
                    id={data.key}
                    handleRemove={this.handleRemove}
                    title={data.title}
                    content={data.content}
                    contact={data.contact}
                    housetype={data.housetype}
                    rentpaytype={data.rentpaytype}
                  />
                ))}
              </div>

            </form>
            </BlockUi>
          </div>
        </div>
      </div>
    );
  }
}
