import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../Main/Home';
import RegisterView from '../Main/RegisterView';
import Login from '../Main/SigninView';
import About from '../Main/About';
import Contact from '../Main/Contact';
import MyPost from '../Posts/MyPost';
import Favourites from '../Posts/Favourites';
import NewPost from '../Posts/NewPost';
import EditPost from '../Posts/EditPost';
import NotMatch from './NotMatch';
import Search from '../Main/Search';

export default () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/register" component={RegisterView} />
    <Route exact path="/login" component={Login} />
    <Route exact path="/about" component={About} />
    <Route exact path="/contact" component={Contact} />
    <Route exact path="/newpost" component={NewPost} />
    <Route exact path="/mypost" component={MyPost} />
    <Route exact path="/favourites" component={Favourites} />
    <Route exact path="/editpost/:id" component={EditPost} />
    <Route exact path="/search" component={Search} />
    <Route  component={NotMatch} />
  </Switch>
);