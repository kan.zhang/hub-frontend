import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './App';
import './styles/index.scss';

import axios from 'axios';

axios.defaults.baseURL = 'http://leasehub.ap-southeast-2.elasticbeanstalk.com/v1';
//axios.defaults.baseURL = 'http://localhost:3888/v1';


ReactDOM.render(
  <Router>
    <App />
  </Router>, 
  document.getElementById('root')
);















// import * as serviceWorker from './serviceWorker';
// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
